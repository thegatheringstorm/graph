#include <iostream>
#include <cassert>
#include <vector>
#include <queue>
#include <algorithm>
#include "Edge.h"

using namespace std;

enum class GraphType {
    DirectedGraph, UndirectedGraph
};
static const int INFINTY = 99999;
struct Vertex {
    int distance;
    int p;
};

bool hasInDegree(const int &index, const vector<vector<Edge>> &AdjacencyMatrix, const int &NumberOfVertex) {
    assert(index < NumberOfVertex);
    for (int i = 0; i < NumberOfVertex; i++) {
        if (AdjacencyMatrix[i][index].isHasEdge()) return true;
    }
    return false;
}

void PrintVector(vector<int> &val) {
    for (const auto &element:val) {
        cout << element << " ";
    }
    cout << endl;
}

void PrintCosts(vector<EdgeInput> &val) {
    for (const auto &element:val) {
        cout << element.x << " " << element.y << " " << element.w << " " << endl;
    }
}

vector<int> findAdjacencyVertex(int source, const vector<vector<Edge>> &AdjacencyMatrix) {
    vector<int> result;
    for (int i = 0; i < AdjacencyMatrix.size(); i++) {
        if (AdjacencyMatrix[source][i].isHasEdge()) result.push_back(i);
    }
    return result;
}

vector<int> TopologicalSorting(vector<vector<Edge>> DirectedGraphAdjacencyMatrix) {
    vector<int> result;
    vector<int> temp;
    const int NumberOfVertex = DirectedGraphAdjacencyMatrix.size();

    while (result.size() < NumberOfVertex) {
        for (int i = 0; i < NumberOfVertex; i++) {
            if (!hasInDegree(i, DirectedGraphAdjacencyMatrix, NumberOfVertex) &&
                (find(result.begin(), result.end(), i) == result.end())) {
                result.push_back(i);
                temp.push_back(i);
            }
        }
        for (auto m:temp) {
            for (auto &e:DirectedGraphAdjacencyMatrix[m]) {
                e.setNoEdge();
            }
        }
        temp.clear();
    }
    return result;
}

EdgeInput MinCostEdge(const vector<EdgeInput> &Costs) {
    assert(Costs.size() > 0);
    EdgeInput result{-1, -1, INFINTY};
    for (const auto &element:Costs) {
        if (element.w < result.w) {
            result = element;
        }
    }
    return result;
}

int findIndexWhichDistanceShortest(const vector<Vertex> &CT, const int &NumberOfVertex, const vector<int> temp) {
    int result{-1};
    Vertex haha{9999, 0};
    for (int i = 0; i < NumberOfVertex; i++) {
        if ((CT[i].distance < haha.distance) && find(temp.begin(), temp.end(), i) == temp.end()) {
            result = i;
            haha = CT[i];
        }
    }
    assert(result != -1);
    return result;
}

vector<vector<Edge>>
ConvertDataToDirectedGraphAdjacencyMatrix(const vector<EdgeInput> &EdgeInputs, const int &NumberOfVertex) {
    vector<vector<Edge>> result(NumberOfVertex, vector<Edge>(NumberOfVertex, Edge()));
    for (const auto &element: EdgeInputs) {
        result[element.x][element.y].setEdge(element.w);
    }
    return result;
}

vector<vector<Edge>>
ConvertDataToUndirectedGraphAdjacencyMatrix(const vector<EdgeInput> &EdgeInputs, const int &NumberOfVertex) {
    vector<vector<Edge>> result(NumberOfVertex, vector<Edge>(NumberOfVertex, Edge()));
    for (const auto &element: EdgeInputs) {
        result[element.x][element.y].setEdge(element.w);
        result[element.y][element.x].setEdge(element.w);
    }
    return result;
}

vector<vector<int>>
ConvertAdjacencyMatrixToAdjacencyList(const vector<vector<Edge>> &AdjacencyMatrix, const int &NumberOfVertex) {
    vector<vector<int>> result(NumberOfVertex, vector<int>());
    for (int m = 0; m < NumberOfVertex; m++) {
        for (int n = 0; n < NumberOfVertex; n++) {
            if (AdjacencyMatrix[m][n].isHasEdge()) {
                result[m].push_back(n);
            }
        }
    }
    return result;
}

void
DFS(const int &index, vector<int> &result, vector<bool> &hasBeenVisited, const vector<vector<int>> &AdjacencyList) {
    if (!hasBeenVisited[index]) {
        hasBeenVisited[index] = true;
        result.push_back(index);
        for (const auto &element:AdjacencyList[index]) {
            DFS(element, result, hasBeenVisited, AdjacencyList);
        }
    }
}


void hasInDegreeTest() {
    int NumberOfVertex = 3;
    vector<EdgeInput> EdgeInputs = {
            {0, 1, 3},
            {1, 2, 6}
    };
    auto DirectedGraphAdjacencyMatrix = ConvertDataToDirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex);
    assert(hasInDegree(1, DirectedGraphAdjacencyMatrix, 3));
    assert(!hasInDegree(0, DirectedGraphAdjacencyMatrix, 3));

}

queue<int> generateInitQueueForBFS(const int &NumberOfVertex, const vector<vector<Edge>> &AdjacencyMatrix) {
    queue<int> result;
    for (int i = 0; i < NumberOfVertex; i++) {
        if (!hasInDegree(i, AdjacencyMatrix, NumberOfVertex)) {
            result.push(i);
        }
    }

    return result;
}

void pushVertexToT(const int VertexIndex, vector<int> &VMinusT, vector<int> &T, vector<EdgeInput> &Costs,
                   const vector<vector<Edge>> &AdjacencyMatrix, const int &NumberOfVertex) {
    VMinusT.erase(find(VMinusT.begin(), VMinusT.end(), VertexIndex));
    T.push_back(VertexIndex);
    for (int i = 0; i < NumberOfVertex; i++) {
        if (AdjacencyMatrix[VertexIndex][i].isHasEdge() && (find(T.begin(), T.end(), i) == T.end())) {
            Costs.push_back(EdgeInput{VertexIndex, i, AdjacencyMatrix[VertexIndex][i].getCost()});
        }
    }
    Costs.erase(std::remove_if(Costs.begin(), Costs.end(), [VertexIndex](EdgeInput n) { return n.y == VertexIndex; }),
                Costs.end());
}

int Prim(const vector<vector<Edge>> &AdjacencyMatrix, const int &NumberOfVertex) {
    int result = 0;
    vector<int> VMinusT;
    for (int i = 0; i < NumberOfVertex; i++) {
        VMinusT.push_back(i);
    }
    vector<int> T;
    vector<EdgeInput> Costs;

    pushVertexToT(0, VMinusT, T, Costs, AdjacencyMatrix, NumberOfVertex);
    while (!VMinusT.empty()) {
        auto val = MinCostEdge(Costs);
        pushVertexToT(val.y, VMinusT, T, Costs, AdjacencyMatrix, NumberOfVertex);
        result += AdjacencyMatrix[val.x][val.y].getCost();
    }
    return result;
}

void PrimTest() {
    int NumberOfVertex = 5;
    vector<EdgeInput> EdgeInputs = {
            {0, 1, 5},
            {0, 2, 6},
            {0, 4, 9},
            {2, 3, 7},
            {3, 4, 8}
    };
    auto UndirectedGraphAdjacencyMatrix = ConvertDataToUndirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex);
    auto val = Prim(UndirectedGraphAdjacencyMatrix, NumberOfVertex);
    assert(val == 26);
}

void pushVertexToTTest() {
    int NumberOfVertex = 5;
    vector<int> T{2, 3};
    vector<int> VMinusT{0, 1, 4, 5};
    vector<EdgeInput> Costs{{3, 2, 7},
                            {3, 4, 8}};
    vector<EdgeInput> EdgeInputs = {
            {0, 1, 5},
            {0, 2, 6},
            {0, 4, 9},
            {2, 3, 7},
            {3, 4, 8}
    };
    auto UndirectedGraphAdjacencyMatrix = ConvertDataToUndirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex);
    pushVertexToT(4, VMinusT, T, Costs, UndirectedGraphAdjacencyMatrix, NumberOfVertex);
    assert(find(VMinusT.begin(), VMinusT.end(), 4) == VMinusT.end());
    assert(find(T.begin(), T.end(), 4) != T.end());
    assert(Costs[1].x == 4);
    assert(Costs[1].y == 0);
    assert(Costs[1].w == 9);

}

void generateInitQueueForBFSTest() {
    int NumberOfVertex = 3;
    vector<EdgeInput> EdgeInputs = {
            {0, 1, 3},
            {2, 1, 6}
    };
    auto DirectedGraphAdjacencyMatrix = ConvertDataToDirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex);
    auto val = generateInitQueueForBFS(NumberOfVertex, DirectedGraphAdjacencyMatrix);
    assert(val.front() == 0);
    val.pop();
    assert(val.front() == 2);
}

vector<int> BFS(const vector<vector<Edge>> &AdjacencyMatrix, const int &NumberOfVertex) {
    vector<int> result;
    int index;
    vector<bool> hasBeenVisited(NumberOfVertex, false);
    auto walkQueue = generateInitQueueForBFS(NumberOfVertex, AdjacencyMatrix);

    while (!walkQueue.empty()) {
        index = walkQueue.front();
        walkQueue.pop();
        if (!hasBeenVisited[index]) {
            hasBeenVisited[index] = true;
            result.push_back(index);
            for (int i = 0; i < NumberOfVertex; i++) {
                if (AdjacencyMatrix[index][i].isHasEdge() && !hasBeenVisited[i]) walkQueue.push(i);
            }
        }
    }
    return result;
}

void GraphTypeTest() {
    GraphType val = GraphType::DirectedGraph;
    assert(val == GraphType::DirectedGraph);
    assert(val != GraphType::UndirectedGraph);
}

void ConvertDataToDirectedGraphAdjacencyMatrixTest() {
    int NumberOfVertex = 3;
    vector<EdgeInput> EdgeInputs = {
            {0, 1, 3},
            {1, 2, 6}
    };
    auto DirectedGraphAdjacencyMatrix = ConvertDataToDirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex);
    assert(DirectedGraphAdjacencyMatrix[0][1].getCost() == 3);
    assert(DirectedGraphAdjacencyMatrix[1][2].getCost() == 6);
    assert(DirectedGraphAdjacencyMatrix[0][1].isHasEdge());
    assert(!DirectedGraphAdjacencyMatrix[0][2].isHasEdge());
}

void ConvertDataToUndirectedGraphAdjacencyMatrixTest() {
    int NumberOfVertex = 3;
    vector<EdgeInput> EdgeInputs = {
            {0, 1, 3},
            {1, 2, 6}
    };
    auto UndirectedGraphAdjacencyMatrix = ConvertDataToUndirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex);
    assert(UndirectedGraphAdjacencyMatrix[0][1].getCost() == 3);
    assert(UndirectedGraphAdjacencyMatrix[1][0].getCost() == 3);
    assert(UndirectedGraphAdjacencyMatrix[1][2].getCost() == 6);
    assert(UndirectedGraphAdjacencyMatrix[0][1].isHasEdge());
    assert(UndirectedGraphAdjacencyMatrix[1][0].isHasEdge());
    assert(!UndirectedGraphAdjacencyMatrix[0][2].isHasEdge());
}

void PrintAdjacencyList(const vector<vector<int>> &AdjacencyList) {
    int i = 0;
    cout << "Print Adjacency List" << endl;
    for (const auto &element :AdjacencyList) {
        cout << i << ":";
        for (const auto &innerElement:element) {
            cout << innerElement << " ";
        }
        cout << endl;
        i++;
    }
}

void ConvertAdjacencyMatrixToAdjacencyListTest() {
    int NumberOfVertex = 3;
    vector<EdgeInput> EdgeInputs = {
            {0, 1, 3},
            {1, 2, 6},
            {0, 2, 5}
    };
    auto AdjacencyList = ConvertAdjacencyMatrixToAdjacencyList(
            ConvertDataToDirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex), NumberOfVertex);
    assert(AdjacencyList.size() == 3);
    assert(AdjacencyList[0][0] == 1);
    assert(AdjacencyList[0][1] == 2);
    assert(AdjacencyList[1][0] == 2);
    assert(AdjacencyList[2].empty());
    PrintAdjacencyList(AdjacencyList);
}

void DFSTest() {
    int NumberOfVertex = 3;
    vector<bool> hasBeenVisited(NumberOfVertex, false);


    vector<EdgeInput> EdgeInputs = {
            {0, 1, 3},
            {1, 2, 6},
            {0, 2, 5}
    };
    auto AdjacencyList = ConvertAdjacencyMatrixToAdjacencyList(
            ConvertDataToDirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex), NumberOfVertex);
    vector<int> result;

    for (int i = 0; i < NumberOfVertex; i++) {
        DFS(i, result, hasBeenVisited, AdjacencyList);
    }

    assert(result.size() == 3);
    assert(result[0] == 0);
    assert(result[1] == 1);
    assert(result[2] == 2);

    cout << "Print DFS Result" << endl;
    PrintVector(result);
}

void BFSTest() {
    int NumberOfVertex = 5;

    vector<EdgeInput> EdgeInputs = {
            {0, 1, 6},
            {2, 1, 6},
            {2, 4, 6},
            {1, 3, 6},
    };

    auto AdjacencyMatrix = ConvertDataToDirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex);

    auto result = BFS(AdjacencyMatrix, NumberOfVertex);
    cout << "Print BFS Result" << endl;

    PrintVector(result);
    assert(result.size() == 5);
    assert(result[0] == 0);
    assert(result[1] == 2);
    assert(result[2] == 1);
    assert(result[3] == 4);
    assert(result[4] == 3);
}

void EdgeInputTest() {
    struct EdgeInput val = {1, 2, 3};
    assert(val.x == 1);
    assert(val.y == 2);
    assert(val.w == 3);
}

void EdgeTest() {
    Edge val{};
    assert(val.isHasEdge() == false);
    val.setEdge(6);
    assert(val.getCost() == 6);
    assert(val.isHasEdge());
}


void findAdjacencyVertexTest() {
    int NumberOfVertex = 5;
    vector<EdgeInput> EdgeInputs = {
            {0, 1, 5},
            {0, 2, 6},
            {0, 4, 9},
            {2, 3, 7},
            {3, 4, 8}
    };
    auto UndirectedGraphAdjacencyMatrix = ConvertDataToUndirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex);
    auto val = findAdjacencyVertex(0, UndirectedGraphAdjacencyMatrix);
    assert(val.size() == 3);
    assert(val[0] == 1);
    assert(val[1] == 2);
    assert(val[2] == 4);
}

void MinCostEdgeTest() {
    vector<EdgeInput> val{
            {3, 2, 7},
            {3, 4, 8}
    };
    assert(MinCostEdge(val).y == 2);
}

void TopologicalSortingTest() {
    int NumberOfVertex = 7;
    vector<EdgeInput> EdgeInputs = {
            {0, 3, 1},
            {2, 1, 1},
            {3, 4, 1},
            {3, 5, 1},
            {3, 6, 1},
            {1, 6, 1},
            {1, 5, 1}
    };
    auto DirectedGraphAdjacencyMatrix = ConvertDataToDirectedGraphAdjacencyMatrix(EdgeInputs, NumberOfVertex);
    auto val = TopologicalSorting(DirectedGraphAdjacencyMatrix);
    cout << "TopologicalSortingTest" << endl;
    PrintVector(val);
}

vector<int> Dijkstra(const int source, const vector<vector<Edge>> &AdjacencyMatrix) {
    vector<Vertex> CT;
    vector<int> temp;
    const int NumberOfVertex = int(AdjacencyMatrix.size());
    for (int i = 0; i < NumberOfVertex; i++) {
        CT.push_back(Vertex{INFINTY, -1});
    }
    vector<int> result(NumberOfVertex, 0);
    CT[source].distance = 0;
    CT[source].p = source;
    int counter = (NumberOfVertex - 1);
    while (counter) {
        int index = findIndexWhichDistanceShortest(CT, NumberOfVertex, temp);
        temp.push_back(index);
        for (const auto &element:findAdjacencyVertex(index, AdjacencyMatrix)) {
            auto AssumeD = CT[index].distance + AdjacencyMatrix[element][index].getCost();
            if (AssumeD < (CT[element].distance)) {
                CT[element].distance = AssumeD;
                CT[element].p = index;
            }
        }
        counter--;
    }
    for (int i = 0; i < NumberOfVertex; i++) {
        result[i] = CT[i].distance;
    }
    return result;

}

void DijkstraTest() {
    vector<EdgeInput> EdgeInputs = {
            {0, 2, 2},
            {0, 1, 1},
            {1, 2, 700}
    };
    auto UndirectedGraphAdjacencyMatrix = ConvertDataToUndirectedGraphAdjacencyMatrix(EdgeInputs, 3);
    auto val = Dijkstra(0, UndirectedGraphAdjacencyMatrix);
    assert(val[0] == 0);
    assert(val[1] == 1);
    assert(val[2] == 2);
    auto val2 = Dijkstra(1, UndirectedGraphAdjacencyMatrix);
    assert(val2[0] == 1);
    assert(val2[1] == 0);
    assert(val2[2] == 3);
}

int main() {
    GraphTypeTest();
    ConvertDataToDirectedGraphAdjacencyMatrixTest();
    ConvertDataToUndirectedGraphAdjacencyMatrixTest();
    ConvertAdjacencyMatrixToAdjacencyListTest();
    hasInDegreeTest();
    generateInitQueueForBFSTest();
    DFSTest();
    BFSTest();
    EdgeInputTest();
    EdgeTest();
    pushVertexToTTest();
//    findIndexWhichDistanceShortestTest();
    findAdjacencyVertexTest();
    MinCostEdgeTest();
    PrimTest();
    TopologicalSortingTest();
    DijkstraTest();
    cout << "Successful" << endl;
    return 0;
}