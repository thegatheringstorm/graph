//
// Created by tgs on 17-11-27.
//

#include "Edge.h"

Edge::Edge() : hasEdge(false) {}


int Edge::getCost() const {
    return cost;
}

void Edge::setEdge(int cost) {
    Edge::cost = cost;
    hasEdge = true;
}

bool Edge::isHasEdge() const {
    return hasEdge;
}

void Edge::setNoEdge() {
    hasEdge = false;
}
