//
// Created by tgs on 17-11-27.
//

#ifndef GRAPH_EDGE_H
#define GRAPH_EDGE_H

struct EdgeInput {
    int x;
    int y;
    int w;
};

class Edge {
public:
    Edge();

public:
    int getCost() const;

    void setEdge(int cost);

    bool isHasEdge() const;
    void setNoEdge();

private:
    int cost;
    bool hasEdge;

};


#endif //GRAPH_EDGE_H
